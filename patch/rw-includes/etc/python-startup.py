import atexit
import os
import readline
from pathlib import Path

# Make tab completion work
readline.parse_and_bind('tab: complete')

# The write_history_file() function fails with
# "OSError: [Errno 16] Device or resource busy" if the history file
# is bind-mounted from the host, so we use a different file and
# copy its contents to the original history file on exit.
history = Path("~/.python_history").expanduser()
tmp_history = Path("~/.python_history.tmp").expanduser()

@atexit.register
def write_history():
    # Write the history to a temporary file
    readline.write_history_file(tmp_history)
    # Copy the history to the original file
    history.write_text(tmp_history.read_text())
    # Remove the temporary file
    tmp_history.unlink()

