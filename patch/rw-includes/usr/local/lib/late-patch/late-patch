#!/bin/bash

set -eu
set -x

SCRIPT_DIR=$(readlink -f "$(dirname "$0")")
INCLUDES_DIR="${SCRIPT_DIR}/rw-includes"

copy_rw_includes() {
  while read -r file; do
    local src="${file}"
    local dest="${file#"${INCLUDES_DIR}"}"

    # Create the target directory
    mkdir -p "$(dirname "${dest}")"

    # Copy the file. Don't preserve ownership (i.e. change ownership to
    # root) to avoid issues like:
    #
    #   sudo: /etc/sudoers.d/zzz_tbb is owned by uid 1000, should be 0
    #
    cp --no-dereference --preserve=all --no-preserve=ownership "${src}" "${dest}"

    # If the file was copied below /home/amnesia, change owner to amnesia
    if [ "${dest:0:13}" = /home/amnesia ]; then
      chown amnesia:amnesia "${dest}"
    fi
  done <<< "$(find "${INCLUDES_DIR}" -type f)"
}

# If this script itself is a mountpoint, we are running in bind-mount mode
if mountpoint -q "$0"; then
  # Map root to amnesia for includes below /home/amnesia
  AMNESIA_HOME="${INCLUDES_DIR}/home/amnesia"
  if [ -d "${AMNESIA_HOME}" ]; then
    bindfs --map="0/1000:@0/@1000" "${AMNESIA_HOME}" "${AMNESIA_HOME}"
  fi

  /mnt/vm-scripts/patch/lib/bind-rw-includes "${INCLUDES_DIR}"
else
  copy_rw_includes
fi
