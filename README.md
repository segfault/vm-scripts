Personal scripts I use for Tails development inside a Tails VM.

# Setup

The scripts assume that this directory is inside a local tails.git 
checkout, so to clone it:

```bash
cd "$TAILS_GIT_REPO"
git clone https://gitlab.tails.boum.org/segfault/vm-scripts.git
```

To ignore the directory in the tails.git repo:

```bash
echo /vm-scripts/ > "$TAILS_GIT_REPO/.git/info/exclude"
```

A symlink to the hook script needs to be created:

```bash
ln -s "$TAILS_GIT_REPO/vm-scripts/hook" "$TAILS_GIT_REPO/hook"
```

The rw-includes directory needs to be owned by the libvirt user:

```bash
sudo ./fix-rw-includes-permissions
```
