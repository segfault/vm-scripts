#!/bin/bash

set -euo pipefail
set -x

if [ "${EUID}" -ne 0 ]; then
  echo >&2 "This script must be run as root"
  exit 1
fi

DIR="$(cd "$(dirname "${BASH_SOURCE[0]}")" && pwd)"

if [ -n "${1:-}" ]; then
  GIT_REPO=$(realpath "$1")
else
  GIT_REPO=$(realpath "${DIR}/..")
fi

# Mount new includes on /lib/live/mount/rootfs/filesystem.squashfs,
# which is used by the unsafe browser to create a fresh chroot.
# This is a read-only filesystem, so we create an overlay first.
FSPATH=/lib/live/mount/rootfs/filesystem.squashfs
if ! findmnt "${FSPATH}" | grep overlay; then
  mountpoint -q /tmp || mount -t tmpfs /tmp
  mkdir -p /tmp/squashfs-upper /tmp/squashfs-work
  mount -t overlay overlay -o lowerdir="${FSPATH}",upperdir=/tmp/squashfs-upper,workdir=/tmp/squashfs-work "${FSPATH}"
fi

"${DIR}/mount-new-includes" "${GIT_REPO}" "${FSPATH}"
